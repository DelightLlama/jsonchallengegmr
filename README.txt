By Francisco Semblert

Unity project to read JSON data from files, tested with .json files in StreamingAssets folder, windows platform
This challenge uses the SimpleJSON Plugin by Bunny83 https://wiki.unity3d.com/index.php/SimpleJSON, 
since Unity's built-in JSON Utility cannot parse the data without knowing the name keys and struct of each entry, and parse nested JSONs.

This solution implements load of the data at the Start of the Scene and thru UI buttons "Reload" and "Next", to manually reload the current file in case the file has changed, 
and to load the next file in the array of file paths that is scanned at Start()

Auto detect if file has changed, isn't implemented.

Given the information of the challenge, the only knowns and constants are the keys "Title", "ColumnHeaders" and "Data", that are string, string array and JSONArray respectively

Since the objective is to display the data, alignment of each data row with the columnheader is not a priority.
Text styling is done thru script using then richtext functionality of the textmesh pro component.