//By Francisco Semblert

//Unity project to read JSON data from files, tested with.json files in StreamingAssets folder, windows platform
//This challenge uses the SimpleJSON Plugin by Bunny83 https://wiki.unity3d.com/index.php/SimpleJSON, 
//since Unity's built-in JSON Utility cannot parse the data without knowing the name keys and struct of each entry, and parse nested JSONs.

//This solution implements load of the data at the Start of the Scene and thru UI buttons "Reload" and "Next", to manually reload the current file in case the file has changed,
//and to load the next file in the array of file paths that is scanned at Start()

//Given the information of the challenge, the only knowns and constants are the keys "Title", "ColumnHeaders" and "Data", that are string, string array and JSONArray respectively

//Since the objective is to display the data, alignment of each data row with the columnheader is not a priority.
//Text styling is done thru script using then richtext functionality of the textmesh pro component.

using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using UnityEngine;
using SimpleJSON;
using TMPro;

public class JsonReader : MonoBehaviour {

	//Some varaibles set to null, to avoid console warnings

	[SerializeField]
	private TextMeshProUGUI _titleText = null, _columnsText = null;

	[SerializeField]
	private GameObject _columnHeadersHolder = null, _rowSample = null;

	[SerializeField]
	private bool _destroyOnReloadLoad = true;

	private int _currentFilePathIndex = 0;

	private readonly string NO_DATA = "no data";

	private string[] _filePaths;

	private List<string> _tableKeys;

	private List<GameObject> _rows;

	private List<TextMeshProUGUI> _columnsTexts;

	private Vector2 _rowsInitialPos;

	private JSONNode _tableNode;

	private JSONTable jsonTable;

	// Use this for initialization
	void Start() {
		//All the files packaged in StreamingAssets are listed 
		List<string> tempFiles = new List<string>(), listFiles = new List<string>(Directory.GetFiles(getPlatformPath));

		foreach (string p in listFiles) {
#if UNITY_EDITOR
			if (!p.Contains(".meta")) {
				tempFiles.Add(p);
			}
#else
			tempFiles.Add(p);
#endif
		}

		//The filtered list is turn into an array of filepaths
		_filePaths = tempFiles.ToArray();

		//The index of file paths is set to 0
		_currentFilePathIndex = 0;
		//The file is read and data displayed

		LoadJSON();
	}

	// Update is called once per frame
	void Update() {

	}

	private void LoadJSON() {
		if (_destroyOnReloadLoad) {
			DestroyColumnTexts();
			DestroyRows();
		}

		//Debug.Log("path: " + filePath);
		if (File.Exists(_filePaths[_currentFilePathIndex])) {
			int i, j, rowLen, colLen;
			float columnWidth;
			string jsonTableString = File.ReadAllText(_filePaths[_currentFilePathIndex]);
			string[] tempArray = null;

			JSONObject tempJSONObj;
			JSONArray jsonColumns, jsonData;

			//Using SimpleJSON plugin
			_tableNode = JSON.Parse(jsonTableString);
			if (_tableNode != null) {
				_tableKeys = new List<string>();
				foreach (KeyValuePair<string, JSONNode> n in _tableNode) {
					//Debug.Log(n.Key);
					_tableKeys.Add(n.Key);
				}
			}

			if (_tableNode.HasKey("Title")) {
				//if to avoid warnings in editor
				if (_titleText != null) {
					_titleText.text = SetStringBold(_tableNode["Title"].Value);
				}
			} else {
				_titleText.text = NO_DATA;
			}

			//To check if columns and data has any values
			jsonColumns = _tableNode["ColumnHeaders"].AsArray;
			jsonData = _tableNode["Data"].AsArray;

			colLen = jsonColumns.Count;
			rowLen = jsonData.Count;

			FreeColumnTextsFromParent();

			if (colLen > 0 && rowLen > 0) {
				//if to avoid warnings in editor
				//the initial position for data rows
				if (_columnHeadersHolder != null) {
					//_columnsHeadersText.text = SetStringItalic(SetStringBold(JoinArray(jsonColumns, _rowSeparator)));
					//_rowsInitialPos = _columnHeadersHolder.transform.position;
					_rowsInitialPos = _columnHeadersHolder.GetComponent<RectTransform>().anchoredPosition;
					_rowsInitialPos.y -= _columnHeadersHolder.GetComponent<RectTransform>().rect.height;
				}

				columnWidth = _columnHeadersHolder.GetComponent<RectTransform>().rect.width / (float)colLen;

				SetupRowsPositions(rowLen);
				CreateTexts((rowLen + 1) * colLen);//Text pool set to the number of rows + the column header, multiplied by the number of columns

				tempArray = new string[colLen];
				for (i = 0; i < colLen; i++) {
					tempArray[i] = SetStringItalic(SetStringBold(jsonColumns[i].Value));
				}
				SetupColumnsTextsTo(_columnHeadersHolder, colLen, columnWidth, tempArray);

				for (i = 0; i < rowLen; i++) {
					tempArray = new string[colLen];
					tempJSONObj = jsonData[i].AsObject;
					for (j = 0; j < colLen; j++) {
						tempArray[j] = tempJSONObj[jsonColumns[j].Value].Value;
					}
					SetupColumnsTextsTo(_rows[i], colLen, columnWidth, tempArray);
				}
			} else {
				HideColumnTexts();
				HideRows();
			}

			//Trying to parse the json without plugins, limited to trying to read the nested JSON array, since the build in json utility cannot do it and by time
			//What is known, is that the JSON contains the keys "Title", "ColumnsHeaders" and "Data", "ColumnHeaders" is an array, "Data" is a nested JSON. but the number of properties and values of the latter two are not

			//The JSON string is parsed from the string in an instance of JSONTable
			jsonTable = JsonUtility.FromJson<JSONTable>(jsonTableString);
			dynamic data = null;
			if (jsonTable.ColumnHeaders != null) {
				//Since the name and number of columns is an unknown, am trying to create a dynamic object creatin the properties at runtime, so the nested JSONArray can be parsed
				data = new ExpandoObject();
				foreach (string column in jsonTable.ColumnHeaders) {
					//The propertity is added to the object with one of the values in jsonTable.ColumnHeaders as it name
					AddProperty(data, column);
					//All properties are initialized as a string "Empty"
					//Debug.Log(column + ": " + ReadProperty(data, column));
				}
			}

			//Here is where i can't continue, JsonUtility can't parse a nested JSON, and if it could, the struct needs to be known beforehand
			if (jsonTable.Data != null) {
				//Trying to parse the data to the dynamic object, but data return as void
				//foreach (string jsonDataString in jsonTable.Data) {
				//if (jsonDataString != null) {
				//data = JsonUtility.FromJsonOverwrite(jsonTableString, data);
				/*if (data != null) {
					data = JsonUtility.FromJsonOverwrite(jsonTableString, data);
					foreach (string column in jsonTable.ColumnHeaders) {
						Debug.Log("\t\t" + column + ": " + data[column]);
					}
				}*/
				//}
				//}
			} else {
				//Debug.Log("Data IS null");
			}
		} else {
			Debug.LogWarning("File not found");
		}
	}

	//function that UI button Reload calls, to reload the current file
	public void OnReloadFile() {
		LoadJSON();
	}

	//function that UI button Next calls, to load the next file in the file paths array
	public void OnNextFile() {
		if (_currentFilePathIndex < (_filePaths.Length - 1)) {
			_currentFilePathIndex++;
		} else {
			_currentFilePathIndex = 0;
		}
		LoadJSON();
	}

	//To add a property to a dynamic object, value is set to a string "Empty"
	private void AddProperty(ExpandoObject expando, string propertyName) {
		var expandoDict = expando as IDictionary<string, object>;
		expandoDict.Add(propertyName, "Empty");
	}

	//To read the property of a dynamic object
	private string ReadProperty(ExpandoObject expando, string propertyName) {
		var expandoDict = expando as IDictionary<string, object>;
		if (expandoDict.ContainsKey(propertyName)) {
			return (string)expandoDict[propertyName];
		} else {
			return "";
		}
	}

	private string SetStringBold(string str) {
		return "<b>" + str + "</b>";
	}

	private string SetStringItalic(string str) {
		return "<i>" + str + "</i>";
	}

	//To join array as a single string, used at the start of the project when column aligment wasn't working
	private string JoinArray(string[] array, string separator) {
		return string.Join(separator, array);
	}

	private string JoinArray(JSONArray array, string separator) {
		int i, len = array.Count;
		string[] tempArray = new string[len];
		for (i = 0; i < len; i++) {
			tempArray[i] = array[i].Value;
		}
		return JoinArray(tempArray, separator);
	}

	private void SetupRowsPositions(int rowCount = 1) {
		//The data row that is used as cloning sample, is activaded
		_rowSample.SetActive(true);

		if (rowCount > 0) {
			if (_rows == null) {
				_rows = new List<GameObject>();
			}

			//data rows are activaded up to data count
			foreach (GameObject row in _rows) {
				row.SetActive(true);
			}

			if (_rows.Count < rowCount) {
				//If the data count is more than instantiaded data rows, more are added to the pool
				while (_rows.Count < rowCount) {
					_rows.Add(Instantiate(_rowSample, _rowSample.transform.parent));
				}
			} else if (_rows.Count > rowCount) {
				//If data count is less than instantiaded data rows, the excess is deactivaded
				for (int i = rowCount - 1; i < _rows.Count; i++) {
					_rows[i].SetActive(false);
				}
			}

			//data rows are activaded up to data count
			for (int i = 0; i < rowCount; i++) {
				_rows[i].SetActive(true);
			}

			//Data rows are repositioned
			Vector3 tempPos;
			foreach (GameObject row in _rows) {
				if (row.activeSelf) {
					if (_rows.IndexOf(row) == 0) {
						//row.transform.localPosition = _rowsInitialPos;
						row.GetComponent<RectTransform>().anchoredPosition = _rowsInitialPos;
					} else {
						tempPos = _rows[_rows.IndexOf(row) - 1].transform.localPosition;
						tempPos.y -= _rows[_rows.IndexOf(row) - 1].GetComponent<RectTransform>().rect.height;
						row.transform.localPosition = tempPos;
					}
				}
			}
		} else {
			HideRows();
		}

		//The data row that is used as cloning sample, is deactivaded
		_rowSample.SetActive(false);
	}

	private void HideRows() {
		if (_rows == null) { return; }

		foreach (GameObject g in _rows) {
			g.SetActive(false);
		}
	}

	private void DestroyRows() {
		if (_rows == null) { return; }

		foreach (GameObject g in _rows) {
			g.transform.parent = null;
			Destroy(g);
		}

		_rows.Clear();
		_rows = null;
	}

	private void CreateTexts(int amount = 1) {
		if (_columnsTexts == null) {
			_columnsTexts = new List<TextMeshProUGUI>();
		}

		if (amount > 0) {
			GameObject tempInst;

			_columnsText.gameObject.SetActive(true);

			foreach (TextMeshProUGUI t in _columnsTexts) {
				t.gameObject.SetActive(true);
			}

			if (_columnsTexts.Count < amount) {
				while (_columnsTexts.Count < amount) {
					tempInst = Instantiate(_columnsText.gameObject);
					_columnsTexts.Add(tempInst.GetComponent<TextMeshProUGUI>());
				}
			} else if (_columnsTexts.Count > amount) {
				for (int i = _columnsTexts.Count - 1; i < amount; i++) {
					_columnsTexts[i].gameObject.SetActive(false);
				}
			}

			_columnsText.gameObject.SetActive(false);
		} else {
			HideColumnTexts();
		}
	}

	private void SetupColumnsTextsTo(GameObject toGO, int columnCount = 0, float columnWidth = 0, string[] texts = null) {
		int textIndex = 0;
		float leftEdge = toGO.GetComponent<RectTransform>().anchoredPosition.x - (toGO.GetComponent<RectTransform>().rect.width / 2f);
		Vector2 pos;

		//Getting the index of columnText that is not in use (no row parent)
		foreach (TextMeshProUGUI t in _columnsTexts) {
			if (t.transform.parent == null) {
				textIndex = _columnsTexts.IndexOf(t);
				break;
			}
		}

		for (int i = 0; i < columnCount; i++) {
			//Asigning parent size and text
			_columnsTexts[textIndex + i].transform.parent = toGO.transform;
			_columnsTexts[textIndex + i].gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, columnWidth);
			_columnsTexts[textIndex + i].text = texts[i];

			//Asigning popsition
			if (i == 0) {
				pos = Vector2.zero;
				pos.x -= (toGO.GetComponent<RectTransform>().rect.width / 2) - (_columnsTexts[textIndex + i].gameObject.GetComponent<RectTransform>().rect.width / 2);
				_columnsTexts[textIndex + i].gameObject.GetComponent<RectTransform>().anchoredPosition = pos;
			} else {
				pos = _columnsTexts[textIndex + (i - 1)].transform.localPosition;
				pos.x += _columnsTexts[textIndex + (i - 1)].GetComponent<RectTransform>().rect.width;
				_columnsTexts[textIndex + i].gameObject.transform.localPosition = pos;
			}
		}
	}

	private void FreeColumnTextsFromParent() {
		//Text are set free from the parent, since the number of columns isn't constant, texts cannot be fixed to the header or row
		if (_columnsTexts != null) {
			foreach (TextMeshProUGUI t in _columnsTexts) {
				t.transform.parent = null;
			}
		}
	}

	private void HideColumnTexts() {
		if (_columnsTexts == null) { return; }

		foreach (TextMeshProUGUI t in _columnsTexts) {
			t.transform.parent = null;
			t.gameObject.SetActive(false);
		}
	}

	private void DestroyColumnTexts() {
		if (_columnsTexts == null) { return; }

		foreach (TextMeshProUGUI t in _columnsTexts) {
			t.transform.parent = null;
			Destroy(t.gameObject);
		}

		_columnsTexts.Clear();
		_columnsTexts = null;
	}

	private string NumericSuffix(int n, int length) {
		string suffix = n.ToString();
		while (suffix.Length < length) {
			suffix = "0" + suffix;
		}
		return suffix;
	}

	//Platform specific path to files packaged in, according to documentation, not tested in any other than windows at teh moment, this get is force of habit
	private string getPlatformPath {
		get {
			string path = "";
#if UNITY_ANDROID
			path = "jar:file://" + Application.dataPath + "!/assets/";
#elif UNITY_IOS
			path = Application.dataPath + "/Raw";
#else
			path = Application.dataPath + "/StreamingAssets";
#endif
			return path;
		}
	}

	private class NestedJSON {
		public string json = "";
		/*
		public string ID = "";
		public string Name = "";
		public string Role = "";
		public string Nickname = "";
		*/
	}

	private class JSONTable {
		public string Title			      = "";
		public List<string> ColumnHeaders = null;
		public NestedJSON[] Data  = null;
	}

}